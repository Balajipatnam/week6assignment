package com.greatlearning.interfaces;
import java.sql.SQLException;
import java.util.List;

import com.greatlearning.bean.Movie;


public interface Notification {


	public List<Movie> getMovies() throws SQLException;

	public int add(Movie movie) throws SQLException;

}
