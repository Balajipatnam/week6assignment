package com.greatlearning.interfaces;

public class NotificationFactory {

	public Notification createNotification(String channel) {
		if (channel == null || channel.isEmpty())
			return null;
		if ("comingmovies".equals(channel)) {
			return new ComingMovies();
		} else if ("moviesinTheater".equals(channel)) {
			return new MoviesInTheaters();
		} else if ("TopRatedIndian".equals(channel)) {
			return new TopRatedIndia();
		} else if ("topRatedMovies".equals(channel)) {
			return new TopRatedMovies();
		}

		return null;

	}

}
